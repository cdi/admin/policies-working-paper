# Leitlinie zur Sicherstellung der langfristigen Verfügbarkeit von Forschungsdaten an der FAU

**Entwurf zur Diskussion** *Stand 25.05.2023* 

> Michael Kohlhase, Laura Albers, Klaus Meyer-Wegener, Marcus Walther

#### Präambel
Diese Leitlinie ergänzt und präzisiert die Forschungsdatenpolicy der FAU. Sie wurde am XXX durch die Mitglieder des FAU Competence Center for Research Data and Information (FAU CDI) beschlossen.

Die Leitlinien werden bei Bedarf durch die Mitglieder des FAU CDI evaluiert und an aktuelle Erfordernisse angepasst, durch sie beschlossen und auf der [FAU CDI-Webseite](https://gitlab.rrze.fau.de/cdi/admin/policies-working-paper) publiziert. 

## 0. Begriffsklärungen

> Die werden in allen Leitlinien gebraucht. Also am besten als eigenes Dokument - oder in unserem sowieso schon entstehenden Glossar, auf das dann verwiesen wird.

1. Ein **(Forschungs-) Datensatz** ist eine unveränderbare, zitierbare Sammlung von Daten und zugehörigen Metadaten zu einem fest umrissenen (Forschungs-) Thema. Datensätze sollen den FAIR-Prinzipien (Findable, Accessible, Interoperable, and Reusable) soweit als möglich gerecht werden. Soweit für den Datenschutz geboten, können Datensätze verschlüsselt gespeichert und archiviert werden.
2. Ein **(Forschungs-) Datenfeed** ist ein Forschungsdatensatz, der sich über die Zeit verändert, weil er sich mit dem fortschreitenden Forschungsstand weiterentwickelt. Zu jedem Zeitpunkt (jedoch nicht rückwirkend) kann ein **Snapshot** genommen werden, dieser ist dann ein Forschungsdatensatz (siehe oben). Ein Datenfeed ist FAIR, wenn seine Snapshots das sind. 

3. In diesem Dokument werden die englischen Begriffe (Steward/Custodian) für Rollen im Forschungsdatenmanagement verwendet, da diese international verwendet werden.

   * Ein **Data Steward** ist verantwortlich dafür, die Qualität und Zweckgerechtigkeit (Fit for Purpose) der Forschungsdaten inklusive ihrer Metadaten aus fachlichen Aspekten sicherzustellen.

   * Ein **Data Custodian** ist verantwortlich für die sichere Verwahrung und Zugangsverwaltung für Forschungsdaten der jeweiligen Organisationseinheit (OE) nach den jeweiligen Bestimmungen. 

## 1. Einleitung

Die FAU ist den Mittelgebern und der guten wissenschaftliche Praxis gegenüber verpflichtet, Forschungsdaten (FD) langfristig zu bewahren und zugänglich zu machen. Forschungsdaten an der FAU entstehen in Abschlussarbeiten, in Projekten und an Professuren/Lehrstühlen. 
Allerdings haben sie alle eine begrenzte Laufzeit: Doktoranden promovieren, Lehrstuhlinhaber gehen in den Ruhestand oder werden an andere Universitäten berufen, etc. Dadurch drohen Forschungsdatensätze zu verwaisen, unbrauchbar zu werden oder sogar ganz verlorenzugehen.

Um die dadurch entstehenden Probleme zu lösen, werden folgenden Massnahmen ergriffen: 

1.  Forschungsdatensätze und -Feeds sollen schon bei ihrer Genese mit umfassenden, maschinenverarbeitbaren *Metadaten* versehen werden. Diese enthalten (zwingend):
  * Informationen zum Data Custodian (der Person/Organisationseinheit, die die Rechte an den Daten hält und für sie verantwortlich ist),
  * Informationen zur Lizensierung der Daten (wer hat wann, wo und unter welchen Bedingungen Zugang/Nutzungsrechte an den Daten),
  * ein **Datentestament**, das regelt, was mit den Daten geschehen soll, wenn der ursprüngliche Data Custodian die FAU verlässt. 
2.  Die Organisationseinheiten (OE) der FAU etablieren ein hierarchisches System von institutionellen Forschungsdaten-Custodians, die in Datentestamenten als Daten-Verantwortliche im Erbfall genannt werden können. 
3. Das FAU CDI bietet administrative und technische Unterstützung im Rahmen der FDM-Grundversorgung.

Dieses System wird im Folgenden beschrieben. 

## 2. Die Probleme

### 2.1 Fallstudien
Um die Probleme zu verstehen, betrachten wir einige typische Fälle. 
1. Eine Professorin geht in den Ruhestand, sie hinterlässt *3n+k* Datensätze, die unterschiedlich stark FAU-intern und extern genutzt werden. Einige müssen (nach den DFG/DOI-Richtlinien) noch mindestens 8 Jahre lang bereitgestellt werden. 
**Problem**: Wann kann welcher Datensatz gelöscht werden? 
2. Ein Doktorand erstellt einen Datensatz im Rahmen seiner Doktorarbeit. Dieser enthält personenbezogene Daten und ist daher verschlüsselt gespeichert. 
**Problem**: Wer kann noch darauf zugreifen, wenn der Doktorand die FAU verlässt?
3. Eine Doktorandin arbeitet an einem Datenfeed (z.B. ein WissKI über Gläser in Museen) und will in ihrer Doktorarbeit den Stand zum Abgabezeitpunkt fixieren, dokumentieren und referenzieren. 
4. In einem Seminar, das von einer Lehrbeauftragten gegeben wird, befassen sich zehn Studierende jeweils mit eigenen Datensätzen (bspw. in WissKI), d.h. sie bauen eine Struktur auf und/oder pflegen Daten ein. **Problem**: Wer entscheidet über den Erhalt oder Löschzeitpunkt dieser Ergebnisse? Wie verhindert man einander widersprechende Testamente?

### 2.2 Konzepte
* Jeder Datensatz/Feed hat einen oder mehrere **Data Custodians**: Personen oder FAU-Organisationseinheiten (FAU-OE), die 
  * die Rechte zur Verwaltung, Veröffentlichung und Weitergabe sowie 
  * die Schlüssel zur Entschlüsselung der Daten haben.
* Verlassen alle Data Custodians eines Datensatzes die FAU, **verwaist** er. In diesem Fall übernimmt eine **zentrale Einrichtung** die Rolle des Custodians.
* Datensätze unterliegen **FD-Constraints**, also Regelungen der FAU, des Freistaates, der Fördergeber und/oder der guten wissenschaftlichen Praxis über die Veröffentlichung und Archivierung der Datensätze. Für jeden Datensatz und Zeitpunkt ergeben sich daraus **FD-Pflichten**. Wir nennen Datensätze mit solchen Pflichten **Pflichtdaten**. Es kann für die FAU-OE opportun sein, Datensätze über die FD-Pflichten hinaus zu archivieren bzw. zugänglich zu machen. Zum Beispiel kann die Veröffentlichung viel genutzter Datensätze der Reputation der FAU-OE dienlich sein. Wir reden hier von **Reputationsdaten**, da sie zur wissenschaftlichen Reputation der OE beitragen (können/sollen).
* Verwaist ein Datensatz, muss der **Erbfall** organisiert werden, d.h. es muss ein neuer Data Custodian (der **Datenerbe**) an der FAU gefunden werden.

## 3. Lösung: Institutionelle Data Custodians

Alle FAU-OE (insbesondere die FAU als ganzes, die Fakultäten, Departments und Lehrstühle/Professuren) benennen jeweils einen Data Custodian, also eine Person, die für die OE die unten genannten Aufgaben übernimmt. Im Moment sind fünf Ebenen geplant: 

Die ersten beiden sind die natürlichen Data Stewards:
1. Projekt - Projektleiter (insbesondere ist jede Promovierende die Leiterin ihres Dissertationsprojekts);
2. Lehrstuhl/Professur - Im Default die Inhaber:in, sonst eine von ihr benannte Person;

Die institutionellen OEen bestellen Data Custodians: 

3. Department - Benennt eine Person;
4. Fakultät - Benennt eine Person;
5. Universität - Das FAU CDI benennt eine Person. 

Benennt eine FAU-OE keinen Data Custodian, tritt die nächsthöhere Ebene ein; diese hat natürlich weniger Fachexpertise und könnte geneigt sein, wichtige Reputationsdaten aus Kostengründen zu löschen. Verlässt ein Data Custodian einer OE die FAU oder die OE, ernennt die OE einen neuen. 

In der Regel soll die Steward-/Custodianship eines Datenpools beim Auflösen einer OE an die (eindeutige) OE auf der nächsthöheren Ebene übergehen. Abweichungen von der Regel oder Zuständigkeiten bei Nichteindeutigkeit (z.B. für Projekte über Department-Grenzen hinweg) müssen im Datentestament spezifiziert werden. 

### 3.1 Aufgaben der Stewards/Custodians/OEs
Die Aufgaben der Data Stewards/Custodians sind auf allen Hierarchiestufen die gleichen: 
1. Turnusmäßige Entscheidung über Archivierung, Veröffentlichung, Löschung oder Transfer der Datensätze der OE; 
2. Sicherstellung der Finanzierung für Reputationsdaten;
3. Autorisierung des Zugriffs externer Parteien gemäß der jeweiligen Lizenzen. Gegebenenfalls Vertraulichkeitsvereinbarungen (NDAs) gemäß FAU CDI-Templates organisieren und/oder Entschlüsselung durchführen.

### 3.2 Implementation
Das FAU CDI etabliert (automatisierte) **Erbfall-Prozeduren**, die beim Ausscheiden von Data Custodians die "Vererbung" von Datenbeständen organisieren.
* Alle Datensätze werden in CRIS modelliert und dokumentiert, samt Datentestamenten. 
* Die Data Custodians der OEen werden (wahrscheinlich im IDM) modelliert. 
* Hat beim Verwaisen eines Datensatzes der designierte Erbe die FAU schon verlassen, fällt sie an die (eindeutige) hierarchisch nächsthöhere OE. 
* Die Nutzung der Datensätze muss einem Monitoring unterliegen, damit Entscheidungen über Löschung oder Erhalt von Reputationsdaten eine empirische Basis haben.
* Schlüssel müssen schon beim Anlegen eines Datensatzes hinterlegt werden, damit diese (besonders bei Pflichtdaten) in unerwarteten Erbfällen nicht unzugänglich werden. Es gibt mehrere Möglichkeiten, dies zu organisieren: 
  * ein zentraler Schlüssel-Obmann oder
  * ein zentrales System, in dem die Data Custodians der OE die Schlüssel ablegen.
* Das FAU CDI organisiert Beispiel-NDAs für Datensätze, um die Data Custodians zu unterstützen.

### 3.3 Finanzierung
Pflichtdatensätze in der FDM-Grundversorgung werden über das FAU CDI-Budget finanziert. Für Datensätze der Projektversorgung (projektspezifische Datenvolumina, die über die Grundversorgung hinausgehen) müssen gesonderte Regelungen/Finanzierungen gefunden werden.

### 3.4. Konsequenzen für die Datentestamente 
Ein Datentestament muss 
* die Lizensierung formal (maschinenverarbeitbar) machen,
* die Rechtelage der Daten klären und dokumentieren:
  * Copyright und Lizensierung der einzelnen Fragmente, sowie
  * Copyright-Übertragungen,
* die Rechteübertragung im Erbfall regeln, insbesondere:
  * die Copyright-Übertragung,
  * das Recht, die Daten zu löschen und
  * das Recht auf Schlüsselnutzung und -weitergabe
* alle diese Aspekte so erklären, dass die initialen Data Stewards sie verstehen und informierte Entscheidungen treffen können.

## 4. Flankierende Maßnahmen

Die folgenden Maßnahmen werden durch das FAU Research Data Steward-/Custodianship möglich und unterstützen die Wirksamkeit.
* Jede Promotion an der FAU (und längerfristig alle Abschlussarbeiten) muss
  * einen Datensatz (oder mehrere) erzeugen und archivieren oder
  * eine Erklärung abgeben, warum keiner entstanden ist.
* Zu jedem Datensatz sind die aktuellen Data Stewards/Custodians als Teil der Metadaten dokumentiert. 

## 5. Offene Fragen

1. Reputationsdaten müssen finanziert werden. Lehrstuhlinhaber, die in den Ruhestand gehen, könnten (wenn das möglich ist) interessiert sein, mit ihren Restmitteln die Erhaltung ihrer Daten zu unterstützen (**Datenvermächtnis**).
