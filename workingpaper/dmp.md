# CDI-Leitlinie zu Datenmanagementplänen im Forschungsdatenzyklus an der FAU

**Entwurf zur Diskussion** *Stand 18. 11. 2021* 

> Dieser Text wurde aus der Forschungsdatenpolicy extrahiert und in einen Draft für eine CDI-Leitlinie überführt. Dieser muss noch diskutiert und eigenständig lesbar gemacht werden. 

> Erster Entwurf von 05.12.2022 - ausführlicher, da nun mehr "Platz" als in der Policy.

#### Präambel
Diese Leitlinie ergänzt und präzisiert die Forschungsdatenpolicy der FAU. Sie wurde am XXX durch die Mitglieder der FAU Competence Unit for Research Data and Information (CDI) beschlossen.

Diese Leitlinien werden im regelmäßigen Turnus durch die CDI evaluiert, an aktuelle Erfordernisse angepasst, durch die Mitglieder der CDI beschlossen und auf der [CDI-Webseite](https://gitlab.rrze.fau.de/cdi/admin/policies-working-paper) publiziert. 

### 1. Datenmanagementpläne
Ein Datenmanagementplan (DMP) beschreibt den geplanten beziehungsweise gelebten Umgang mit Forschungsdaten, die in einem Projekt produziert oder genutzt werden.  Dabei werden sowohl die Projektlaufzeit als auch die Zeit nach Abschluss des Projektes betrachtet. 
Typischerweise dokumentieren DMP 
* Regelungen, die innerhalb des Projektteams vereinbart und angewendet werden, 
* die (geplante) Erhebung und Verarbeitung der Daten,
* das Vorgeben bei Speicherung und Transfer der Daten zwischen Projektpartnern,
* Überlegungen zur Veröffentlichung und langfristigen Aufbewahrung der Daten,
* rechtlichen Randbedingungen, die beachtet werden müssen und die daraus abgeleitete Maßnahmen,
* die jeweils verantwortlichen Personen
* und die dazu nötigen Ressourcen.

### 2. Empfehlung für Datenmanagementpläne an der FAU 

DMPs sind ein Baustein der transparenten Dokumentation von Forschungsprozessen. Darüber hinaus erwarten einige Forschungsförderer einen DMP als Teil der zur Begutachtung eingereichten Unterlagen (etwa das AHRC oder bestimmte BMBF-Förderlinien) oder als *Deliverable* während der Projektlaufzeit, beispielsweise bei ERC Grants. 
Die FAU empfiehlt daher Forschungsprojekten grundsätzlich die Voraberstellung eines DMP, um einen systematischen Umgang mit den Forschungsdaten zu unterstützen, sowie die Beantragung der für das Datenmanagement notwendigen Fördermittel zu erleichtern. DMP sollen regelmäßig auf Aktualität geprüft und bei Änderungen in der Planung oder im praktizierten Datenmanagement entsprechend angepasst werden. 

### 3. Unterstützung bei der Erstellung 

Eine erste Erstellung eines DMP kann entlang von Fragenkatalogen erfolgen, die zentrale Aspekte des FDM abfragen. So wird vermieden, dass wichtige Themen übersehen werden. Dies ist bei DMP für Förderer von besonderer Bedeutung, da diese meist konkrete Vorstellungen zum Inhalt und Aufbau der Pläne haben. Die [UB] berät in Kooperation mit der [CDI] bei der Erstellung von DMPs.

Die FAU stellt mit [RDMO](https://rdmo.ub.fau.de) ein Webtool mit DMP-Fragenkatalogen zur Verfügung. Der Fokus liegt auf DMP zur Vorbereitung von DFG-Anträgen und für größere EU-Projekte. Wenn Sie Fragen zur Verwendung von RDMO haben, kontaktieren Sie die [Universitätsbibliothek](mailto:ub-fdm@fau.de).  

Wenn ein DMP für die interne Verwendung im Projekt entwerfen werden soll, der nicht auf bestimmte Förderer zielt, stellt der [Fragebogen von Science Europe](https://doi.org/10.5281/zenodo.4915861) einen guten Ausgangspunkt dar. 

Für bestimmte Fächer / Forschungsförderer sind diese DMP-Online-Werkzeuge empfehlenswert:
* [DataWiz](https://datawiz.leibniz-psychology.org/DataWiz/): Plattform des ZPID; speziell an Fragestellungen in der **Psychologie** angepasst.
* [GFBio Data Management Plan Tool](https://www.gfbio.org/plan): speziell für die Bereiche **Biodiversität / Lebenswissenschaft**, kann auch in verwandten Disziplinen hilfreich sein.
* [DMPOnline](https://dmponline.dcc.ac.uk/): Verfügt über eine große Auswahl an Vorlagen von Forschungsförderern aus dem **Vereinigten Königreich**.

[hier kann man noch ergänzen]


