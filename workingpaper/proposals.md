# CDI-Guided -  Basic RDM text fragments for proposals

Version  of 18.11.2022;  *Work in Progress* 

> Michael Kohlhase, Marcus Walther, Jürgen Rohrwild 

This guide gives you text fragments that can be used for starting the RDM parts of your
next proposal. They can only serve as ideas for formulations and remind you of aspects you
may otherwise forget. Naturally - every research proposal is different - they need to be
adapted to your particular needs and situation. 

This guide will be updated regularly, please feel free to contribute!

## 1. Introduction

Research data management is very discipline-specific and closely bound to the actual research. Therefore you cannot just copy and paste the proposed text elements, sometimes additions by you are necessary. Also not all RDM services may be relevant - if you do not develop software, you do not need version-controlled storage.

This document does not cover concepts for data-management plans themselves. For them, FAU offers the RDMO tool (https://rdmo.ub.fau.de/).

## 2. Glossary

* *Hot Data* - Data that are used "now" in ongoing research. May change often, and a
  regular and easy access is needed. Basically you work on or with these data in your
  daily routines.
* *Warm Data* - Data that is not fixed yet, but is already shared with a selected group
    of researchers (e.g. in the project). 
* *Cold Data* - Data that were used during research (not necessarily for
  publications). They need to be kept to ensure good scientific practice and for future
  work. They are not changed anymore and should be seen as read-only.

## 3. Data-Plan Text Fragments by Research Style

### 3.0 General Clause According to the FAU Research Data Policy

> Data and metadata will be stored at ... (see below) ... and in compliance with the FAU data policy (https://www.fau.info/fdm-policy) as well as the FAIR principles (https://www.go-fair.org/fair-principles/). 

**You will also have to address the following**:
* Say something about publications.
* What else can be added from the RD policy? 

### 3.1 Research/Project Style

Depending on the relevance of data for your research, your data-management plan may need
to provide more information on research-data management.

#### 3.1.1 Data-agnostic Research

> My/Our research does not rely on or produce research data in any significant way. 

#### 3.1.2 Data-driven Research

> My/Our research relies on data produced in the project itself and on various external data sources and processes (see table XX). 

(continued below)

**You will aso have to address the following**: 
* Add a table with an overview of data types, formats and volume.
* Add an overview of data workflow(s) (figure or text).

#### 3.1.3 Data-producing Research

> My/Our research produces research data as a main outcome and will be evaluated on the scope, quantity, and quality of the data supplied to the research community. A high degree of implementation of the FAIR criteria in the produced datasets will be a determining factor for the utility and recognition. Therefore, professional research-data management is vital for quality and success of my/our research work.

#### 3.1.4 Data-intensive Research (replaced "heavy"; still looking for a better name)

> My/Our research is both *data-driven* and *data-producing*. [... explanation ...]. Therefore it ...

**You will aso have to address the following**: 
 Prepare a synopsis text between the two above, when they have stability.

### 3.2 Data Privacy Concerns

#### 3.2.1 Personal Data

> Some/all of the data produced in the project are subject to data-privacy restrictions and cannot be made public. 

#### 3.2.2 Intellectual Properties

...

### 3.3 Data Licensing

#### 3.3.1 Full Data Disclosure

> All data produced in the project will ...

### 3.4 Data Storage

Storage infrastructure needs to be professional. Usually this is fulfilled for *hot data* by
* a central network storage (FAU: RRZE, UKER: MIK),
* discipline-specific data sources,
* 3rd party professional storage systems.
{: .alert .alert-warning}:::

### 3.5 Long-Term Sustainability of Research Data

> After the termination of the proposed project, all relevant datasets will be safeguarded ...

#### 3.5.1 General 

> FAU will ensure the long-term archiving of the research data via its Competence Center for Research Data and Information (CDI, https://cdi.fau.de).

#### 3.5.2 WissKI

> FAU CDI provides hosting and maintenance for WissKI instances as part of the FAUWissKICloud (https://wisski.data.fau.de) Service.

#### 3.6 In-Kind Contributions

Sometimes the funding agency requires an in-kind contribution from FAU for the project (especially large-scale projects like GRK and SFB). The services of CDI and FAUDataCloud can be counted as such. The following text fragments can be used to quantify the in-kind contribution. 

> The in-kind contribution of FAU includes research-data-management support via the CDI and the FAUDataCloud ... 

**You will aso have to address the following**: 
Any in-kind-contribution promises need to be cleared with the CDI, which will record the numbers for internal planning, accounting, and justification. 

## 4. Full Examples

These texts are just an inspiration for you.

### 4.1 Draft Proposal for a Data-agnostic RTG

...

### 4.2 Single Funding DFG Proposal

...

### 4.3 Long-Term Project (e.g. Bavarian Academy of Sciences and Humanities)

> The research data produced in the project mainly consist of digitizations of cultural artefacts and their annotations and metadata in a Linked-Open-Data (LOD) format with standardized ontologies. All research data will be curated in accordance to the respective current FAIR standards and will be published under permissive licences (Open Data Licenses) while respecting the first-publication interests of the project members. 

> FAU will ensure long-term archiving of the research data via its Competence Center for Research Data and Information (CDI, https://cdi.fau.de). FAU CDI provides hosting and maintenance for WissKI instances as part of the FAUWissKICloud (https://wisski.data.fau.de) service and will support the WissKI-based Virtual Research Environment (VRE) of the XXX project after the project termination. 

> To support the continued development of the XXX-VRE after the project duration, we plan to establish an independent organizational structure or foundation (Förderverein). If established during the project duration, this foundation could also supply research associates access to research data and resources with restricted licenses under suitable non-disclosure-agreements (NDA), which could use the Prometheus image archive (https://www.prometheus-bildarchiv.de/) as a role model. 

### 4.4 Eigenanteil
 
> Die Unterstützung des XXX-Projekts im Rahmen der CDI und der FAUDataCloud stellt einen erheblichen Eigenanteil seitens der FAU dar. Das zentrale WissKI-Hosting inkl. Wartung in der FAUWissKICloud sowie die Konfigurationsberatung der CDI schlagen über die Projektlaufzeit mit nur ca. 3 PM zu Buche. Die Bereitstellung der Hardware-Ressourcen (virtuelle Maschine, hochverfügbarer Speicher, Backup, Archivierung) im Rahmen der FAUDataCloud beläuft sich auf ca. 1 PM und einen Hardware-Anteil von ca. 10.000 Euro. Ohne diese zentrale Unterstützung müsste das XXX-Projekt den Betrieb des WissKI-Servers sowie dessen Wartung, Absicherung und Konfiguration selbst organisieren. Dafür müssten mindestens 12 PM + 20.000 Euro zusätzlich für Hardware und Betriebskosten über die Projektlaufzeit gerechnet werden. Ein besonderer Vorteil der genannten Leistungen der FAU liegt weiterhin darin, dass Projektstart und -fortschritt nicht durch die Suche nach und anschließender Einarbeitung von entsprechend qualifiziertem Personal beeinflusst/gefährdet/verzögert wird.

> FAU's own contributions based on CDI services and FAUDataCloud add a huge value to XXX project. Centralized WissKI hosting and maintenance in the FAUWissKICloud context require 3 PM only. Server hardware (virtual machine, storage, backup, archiving) and its administration in the FAUDataCloud can covered by approx. 1 PM and 10.000 Euro for the server pool. Without this central support, XXX project would have to add at least another 12 PM and 20.000 Euro hardware costs for WissKI servers, their configuration, operation, maintenance, and security measures. So, XXX project need not hire and train personnel for those tasks. This is an enormous benefit for the project's start-up phase and further progress.

## 5. Steinbruch

- [Checkliste FDM](https://pad.gwdg.de/LKo70Af6S1ihYZwzLTzFeQ#)
- Wer ist verantwortlich bei einer Gruppe von Forschenden?
- Wo genau erfolgt die Datenspeicherung / Backup (vorzugsweise am RRZE oder ähnlich zentral)? (passt in den obigen Einstiegssatz, wenn dieser verwendet wird)
- Welche Datenarten und welches Datenvolumen fallen an (Modelldaten, Messdaten, Software, ... )?
- Wo erfolgt die Langzeitarchivierung und Veröffentlichung von Datensätzen ([RADAR](https://www.radar-service.eu/), via UB FAU), Zenodo, fachlich)?
- Datenaustausch mit externen Partnern (FAUbox?).
- Welche Fachgesellschaften und NFDI-Konsortien setzen (aktuell und zukünftig) Standards für Forschungsdaten (z.B. NFDI4Ing)?
- Optional: Sie könnten für den Vollantrag die Erstellung eines Datenmanagementplans in Aussicht stellen (müssten das dann aber auch tun), alternativ den DMP erst nach Bewilligung erstellen (oder das Thema nicht ansprechen, da derzeit noch nicht verpflichtend z.B. bei GRKs)
- Beratung an der FAU durch die Competence Unit for Research Data and Information (CDI) – zentrale wissenschaftliche Einrichtung inkl. Bibliothek und Rechenzentrum.
- FAU stellt die Verfügbarkeit der Forschungsdaten auch über die Laufzeit des Projekts hinaus sicher.

<!--  LocalWords:  WissKI-Hosting
 -->
