# CDI-Leitlinie zur Speicherung von Forschungsdaten

**Entwurf zur Diskussion** *Stand 18. 11. 2021* 

> Dieser Text wurde aus der Forschungsdatenpolicy extrahiert und in einen Draft für eine CDI-Leitlinie überführt. Er muss noch diskutiert und eignständig lesbar gemacht werde. 

#### Präambel
Diese Leitlinie ergänzt und präzisiert die Forschungsdatenpolicy der FAU. Sie wurde am XXX durch die Mitglieder der FAU Competence Unit for Research Data and Information (CDI) beschlossen.

Die Leitlinien werden im regelmäßigen Turnus durch die CDI evaluiert, an aktuelle Erfordernisse angepasst, durch die Mitglieder der CDI beschlossen und auf der [CDI-Webseite](https://gitlab.rrze.fau.de/cdi/admin/policies-working-paper) publiziert. 

### 1. Grundsätze zur Speicherung von digitalen Forschungsdaten

1.	Die Speicherung von digitalen Forschungsdaten (dFD) erfolgt nach Möglichkeit während des gesamten Forschungsprozesses in redundant gesicherten Storagesystemen. Das Regionale Rechenzentrum Erlangen (RRZE) unterstützt und berät bei Datensicherung und Backup. 
2.	Die langfristige Ablage von dFD sollte in etablierten, fachspezifischen Repositorien oder Datenzentren erfolgen. Es wird empfohlen, sich vorab über in Frage kommende Repositorien und gegebenenfalls deren spezielle Anforderungen für Archivierung oder Publikation der dFD zu informieren. 
3.	Die Universität stellt für diejenigen Forschenden, in deren Fachdisziplinen sich noch keine Repositorien etabliert haben, einen Zugang zu Diensten und Einrichtungen für Lagerung, Sicherung und Aufbewahrung von dFD bereit. Mit der FAUDataCloud steht an der FAU ein zentrales, kuratiertes Datenarchiv für mittlere Datenvolumen zur Verfügung.
4.	Für das Format der dFD sind bevorzugt freie Standardformate oder in der Fachcommunity etablierte Formate zu wählen, um die Interoperabilität zu erleichtern und eine langfristige Lesbarkeit zu gewährleisten. 
5.	Die für eine Publikation verwendeten dFD sind so lange aufzubewahren, wie es nach einschlägigen gesetzlichen oder vertraglichen Vorschriften, insbesondere nach dem Patentrecht oder nach einer Vorgabe des Drittmittelgebers erforderlich ist, in der Regel mindestens jedoch für zehn Jahre. Im Fall der Weiterverarbeitung von dFD sollen auch die Primärdaten aufbewahrt werden, um bei Bedarf verfügbar zu sein. Sofern nachvollziehbare Gründe dafür existieren, bestimmte Forschungsdaten nicht aufzubewahren, dokumentieren die Wissenschaftlerinnen und Wissenschaftler dies.
6.	Die Qualität der Metadaten beeinflusst maßgeblich die Auffindbarkeit und Nachnutzbarkeit der Daten. Basale Informationen sind beispielsweise beschreibende Titel und Abstracts, beteiligte Forschende, Institution, Identifier, Ort und Zeitraum, Rechte, Formate etc. Die Nutzung standardisierter Metadatenschemata stellt eine möglichst einheitliche und nachvollziehbare Beschreibung sicher und unterstützt die Maschinenlesbarkeit und Interoperabilität. 
7.	Das Forschungsinformationssystem CRIS der FAU sollte genutzt werden, um auch dezentral in externen Repositorien gespeicherte dFD zentral zu erfassen. Als wichtige Ergebnisse universitärer Forschung können diese so hochschulweit dokumentiert und gewürdigt werden. 	
8.	Die Auswahl, Aufbereitung und Dokumentation der Daten sowie die Speicherung über einen definierten Zeitraum benötigen ausreichende Ressourcen. Dies sollte bereits in der Planungsphase berücksichtigt werden; die FAU bietet eine entsprechende Beratung an.
