# CDI-Leitlinie zur Dokumentation und Verwendung von Metadaten für digitale Forschungsdaten an der FAU

**Entwurf zur Diskussion** *Stand 18.11.2021* 

> Dieser Text wurde aus der Forschungsdatenpolicy extrahiert und in einen Draft für eine CDI-Leitlinie überführt. Er muss noch diskutiert und eigenständig lesbar gemacht werde. 

#### Präambel
Diese Leitlinie ergänzt und präzisiert die Forschungsdatenpolicy der FAU. Sie wurde am XXX durch die Mitglieder der FAU Competence Unit for Research Data and Information (CDI) beschlossen.

Die Leitlinien werden im regelmäßigen Turnus durch die CDI evaluiert, an aktuelle Erfordernisse angepasst, durch die Mitglieder der CDI beschlossen und auf der [CDI-Webseite](https://gitlab.rrze.fau.de/cdi/admin/policies-working-paper) publiziert. 

### 1. Grundsätze zur Dokumentation für digitale Forschungsdaten an der FAU

1. Der Entstehungskontext der dFD, Kontextinformationen zu Werkzeugen sowie die verwendete Software, die Analyseprotokolle und der Forschungsprozess an sich sind so zu dokumentieren, dass die FAIR-Prinzipien möglichst umfassend umgesetzt werden. 
2. Ebenso wird die Herkunft verwendeter oder nachgenutzter Materialien, Daten oder Software kenntlich gemacht. Es sollen dabei alle vertretbaren Anstrengungen unternommen werden, um Korrektheit, Unverfälschtheit und Vollständigkeit der Aufzeichnungen und der zugrundeliegenden Daten langfristig sicherzustellen.
3. Neben der zuverlässigen Speicherung der Daten selbst ist die detaillierte Dokumentation eine zentrale Grundvoraussetzung für die Verifizierbarkeit geleisteter Forschungsarbeit und für die Nachnutzbarkeit der Daten im Rahmen neuer Forschungsfragen.
3. Die Dokumentation sollte daher den Anspruch haben, dass die Forschung der FAU von Expertinnen und Experten aus den entsprechenden Fachdomänen grundsätzlich vollständig nachvollzogen werden kann.

## 2. Verwendung von Metadaten für digitale Forschungsdaten an der FAU

> Hier fehlt noch alles. 
