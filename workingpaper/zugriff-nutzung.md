# CDI-Leitlinie zu Zugriffs- und Nutzungsbedingungen für digitale Forschungsdaten an der FAU

**Entwurf zur Diskussion** *Stand 18.11.2021* 

> Dieser Text wurde aus der Forschungsdatenpolicy extrahiert und in einen Draft für eine CDI-Leitlinie überführt. Er muss noch diskutiert und eigenständig lesbar gemacht werde. 

#### Präambel
Diese Leitlinie ergänzt und präzisiert die Forschungsdatenpolicy der FAU. Sie wurde am XXX durch die Mitglieder der FAU Competence Unit for Research Data and Information (CDI) beschlossen.

Die Leitlinien werden im regelmäßigen Turnus durch die CDI evaluiert, an aktuelle Erfordernisse angepasst, durch die Mitglieder der CDI beschlossen und auf der [CDI-Webseite](https://gitlab.rrze.fau.de/cdi/admin/policies-working-paper) publiziert. 

### 1. Grundsätze für Zugriffs- und Nutzungsbedingungen für digitale Forschungsdaten

1.	Es wird grundsätzlich empfohlen, dFD der (Fach-)Öffentlichkeit zur Nachnutzung zugänglich zu machen. 
2.	Um die langfristige Zitierbarkeit der dFD sicherzustellen, wird die konsequente Verwendung von persistenten Identifikatoren nachdrücklich empfohlen. 
3.	In Abhängigkeit von rechtlichen und vertraglichen Anforderungen sind für die Veröffentlichung der dFD der geeignete Zeitpunkt, Umfang und passende Lizenzbedingungen zu wählen. Die Veröffentlichung soll unter dem Namen der Schöpferinnen und Schöpfer erfolgen.
4.	Bei einer Übertragung von Nachnutzungs- oder Veröffentlichungsrechten soll darauf geachtet werden, dass die dFD für wissenschaftliche Zwecke frei verfügbar bleiben. Grundsätzlich sollen permissive Lizenzen, wie z.B. CC oder open data für veröffentlichte dFD gewählt werden, die es erlauben, auf die Daten zuzugreifen, in anderen Kontexten darauf aufzubauen und sie uneingeschränkt weiterzuverbreiten: Forschungsdaten sollen so offen wie möglich sein, aber so geschützt wie nötig!.
5.	Es soll Forschenden ermöglicht werden, ihre nach dieser Policy sowie aus Verträgen mit Drittmittelgebern und aus sonstigen Rechtsquellen bestehenden Rechte und Pflichten wahrzunehmen.
