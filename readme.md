# CDI policy and working paper directory

## Content

This repository contains versioned policy documents and working papers of FAU's Competence Unit for Research Data and Information (CDI).
### 1. CDI-Policies (in the making)
 Extensions of the FAU RD policy managed by the CDI
* [CDI-Leitlinie zur Sicherstellung der langfristigen Verfügbarkeit von Forschungsdaten an der FAU](workingpaper/data-steward-custodian.md)
* [CDI-Leitlinie zur Speicherung von Forschungsdaten](workingpaper/storage.md)
* [CDI-Leitlinie zu Datenmanagementplänen im Forschungsdatenzyklus an der FAU](workingpaper/dmp.md)
* [CDI-Leitlinie zu Zugriffs- und Nutzungsbedingungen für  digitale Forschungsdaten an der FAU](workingpaper/zugriff-nutzung.md)

### 2. CDI-Guides 
Non-normative (but helpful guides and howtos)

* [Basic RDM text for proposals](workingpaper/proposals.md)

### 3. CDI Working Papers 
Concepts, whitepapers, and Work-in-progress resources


## Changes

Please feel free to comment them by creating issues or merge requests. Changes are accepted by either CDI's board or CDI members.

